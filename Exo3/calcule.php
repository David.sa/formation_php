<?php
include("common/header.php");
include("common/menu.php");
?>

<h1>Cercle - perimetre et aire</h1>
<form action="#" method="POST">
    <label for="rayon">Rayon d'un cercle </label>
    <input type="number" name="rayon" id="rayon"><br>
    <label for="perimetre">Perimetre :</label>
    <input type="checkbox" name="perimetre" id="perimetre" checked><br>
    <label for="aire">Aire :</label>
    <input type="checkbox" name="aire" id="aire"><br>
    <input type="submit" value="Envoyer">
</form>

<?php

$r = $_POST["rayon"];

function perimetre($x){
    return $x * 2 * pi();
}

function aire($x){
    return pi() * ($x * $x);
}

if(isset($r) && $r >0){
    echo "<h2>resultat</h2></br>";
    if (isset($_POST["perimetre"])){
      echo perimetre($r);
    }
    elseif(isset($_POST["aire"])){
       echo aire($r);
    }
} else {
    echo "<h2>Saisir une valeur dans le champ ci-dessus</h2>";
}



?>

<?php
include("common/footer.php");
?>


