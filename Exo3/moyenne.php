<?php
include("common/header.php");
include("common/menu.php");

?>

<h1>Calculer une moyenne </h1>

<form action="#" method="GET">
    <label for="notes">Nombre de notes :</label>
    <input type="number" name="notes" id="notes">
    <input type="submit" value="Ajouter">
</form>

<?php

$notes = (int)$_GET["notes"];

if(isset($notes) && $notes > 0){

    echo "<form action=\"#\" method=\"POST\">";
    echo "<fieldset>";
    echo "<legend>Moyenne : </legend>";
    for($i = 1; $i <= $notes ; $i++){
        echo "<label for=\"note".$i."\">Note". $i ." : </label>";
        echo "<input type=\"number\" name=\"note".$i."\" id=\"note".$i."\" required><br/>";
    }
    echo "<input type=\"submit\" value=\"Calculer\">";
    echo "</fieldset>";
    echo "</form>";
    if(isset($_POST['note1'])){
        $res = 0;
        for($i = 1; $i <= $notes ; $i++){
            $res += $_POST['note'.$i];
        }
        echo "La moyenne est : ". $res / $notes;
    }
} else {
    echo "<h2>Saisir une valeur dans le champ ci-dessus</h2>";
}


?>


<?php
include("common/footer.php");
?>




